# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
### Fixed
- When no copus specified in config, a default disabled cop is returned instead of panicking


## 1.11.0
### Added
- GetterFunc - convert functions with appropriate signature into Getter (useful for tests when you want to read config from map)

## 1.10.1
### Fixed
- sort strategy for `pgdb.SortedOffsetPageParams` based on json api

## 1.10.0
### Added
- offset-based page params with sort

## 1.9.0
### Added
pgdb Events Listener that allows you to subscribe to insert,updates,deletes on PostgreSQL DB

## 1.8.0
### Added
- pq listener for databaser

## 1.6.2
### Fixed
- default values for `pgdb.CursorPageParams`

## 1.6.1
### Fixed
- `pgdb.CursorPageParams` urlval tag names

## 1.6.0

### Changed
* vendor dependencies

## 1.5.0

### Removed
* graylog

### Added
* traefik

## v1.4.0

- make page params compatible with `urlval` package

## v1.3.0
### Added
- ValidateLazyDep - allows to ensure that none of the methods panics

## v1.2.0

### Added

* JSONScan and JSONValue methods

## v.1.0.0

### Changed

* Switched to normal versions


## v0.12.0

### Fixed

* DB performance issue. Pass MaxIdleConnections, MaxOpenConnections to the sql.DB


## v0.11.0

### Added

* Cursor-based page params

## v0.10.1

### Fixed

* Typo

## v0.10.0

### Added

* RawDB to pgdb comfig helper

## v0.9.1

### Added

* offset-based page params

## v0.9.0

### Added

* support for `graylog` by `comfig.Log`

### Removed

* `comfig.Databaser` in favor of `pgdb.Databaser`
* `comfig.NewDatabaser` in favor of `pgdb.NewDatabaser`
* `comfig.Januser` in favor of `janus.Januser`
* `config.NewJanuser` in favor of `janus.NewJanuser`
