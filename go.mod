module gitlab.com/distributed_lab/kit

go 1.14

require (
	github.com/Masterminds/squirrel v1.5.4
	github.com/getsentry/sentry-go v0.27.0
	github.com/go-chi/chi v1.5.5
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.9
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.9.3
	github.com/spf13/viper v1.18.2
	github.com/stretchr/testify v1.8.4
	gitlab.com/distributed_lab/figure v2.1.2+incompatible // indirect
	gitlab.com/distributed_lab/figure/v3 v3.1.4
	gitlab.com/distributed_lab/logan v3.8.1+incompatible
	gitlab.com/distributed_lab/running v1.6.0
)
